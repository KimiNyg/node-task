# Node Task

This project contains node
hello-world

## Installation steps

1. Copy the repository
2. Install depencies
3. Start the app

## Terminal commands

Code block:

```sh
git clone
cd node-task
npm i
```

Start cmd: `npm run start`

## App should work

!["simpson"](https://compote.slate.com/images/697b023b-64a5-49a0-8059-27b963453fb1.gif)